class CreateRuns < ActiveRecord::Migration[7.0]
  def change
    create_table :runs do |t|
      t.string :name, null: false
      t.references :court, foreign_key: true
      t.datetime :starts_at
      t.datetime :ends_at
      t.integer :player_limit
      t.datetime :sign_up_opens_at
      t.datetime :late_drop_deadline
      t.integer :price_in_cents
      t.references :run_schedule, foreign_key: true

      t.timestamps
    end
  end
end
