class CreateRunSchedules < ActiveRecord::Migration[7.0]
  def change
    create_table :run_schedules do |t|
      t.string :name, null: false
      t.references :court, foreign_key: true
      t.integer :days_of_week, array: true
      t.time :start_time
      t.time :end_time
      t.integer :player_limit
      t.string :sign_up_open_day
      t.time :sign_up_open_time
      t.integer :late_drop_deadline_day
      t.time :late_drop_deadline_time
      t.integer :price_per_run_in_cents
      t.string :repeat_interval
      t.daterange :repeat_duration

      t.timestamps
    end

    add_index :run_schedules, :days_of_week, using: 'gin'
  end
end