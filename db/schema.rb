# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_04_25_220007) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "courts", force: :cascade do |t|
    t.string "name", null: false
    t.string "street_address"
    t.string "city"
    t.string "state"
    t.string "zip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_courts_on_name", unique: true
  end

  create_table "reservations", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "run_id", null: false
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["run_id"], name: "index_reservations_on_run_id"
    t.index ["user_id"], name: "index_reservations_on_user_id"
  end

  create_table "run_schedules", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "court_id"
    t.integer "days_of_week", array: true
    t.time "start_time"
    t.time "end_time"
    t.integer "player_limit"
    t.string "sign_up_open_day"
    t.time "sign_up_open_time"
    t.integer "late_drop_deadline_day"
    t.time "late_drop_deadline_time"
    t.integer "price_per_run_in_cents"
    t.string "repeat_interval"
    t.daterange "repeat_duration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["court_id"], name: "index_run_schedules_on_court_id"
    t.index ["days_of_week"], name: "index_run_schedules_on_days_of_week", using: :gin
  end

  create_table "runs", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "court_id"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.integer "player_limit"
    t.datetime "sign_up_opens_at"
    t.datetime "late_drop_deadline"
    t.integer "price_in_cents"
    t.bigint "run_schedule_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["court_id"], name: "index_runs_on_court_id"
    t.index ["run_schedule_id"], name: "index_runs_on_run_schedule_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name", null: false
    t.string "last_name", null: false
    t.string "nickname"
    t.string "email", null: false
    t.string "phone"
    t.string "password_digest"
    t.boolean "commish", default: false, null: false
    t.boolean "admin", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "reservations", "runs"
  add_foreign_key "reservations", "users"
  add_foreign_key "run_schedules", "courts"
  add_foreign_key "runs", "courts"
  add_foreign_key "runs", "run_schedules"
end
