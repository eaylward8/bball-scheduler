# == Schema Information
#
# Table name: runs
#
#  id                 :bigint           not null, primary key
#  ends_at            :datetime
#  late_drop_deadline :datetime
#  name               :string           not null
#  player_limit       :integer
#  price_in_cents     :integer
#  sign_up_opens_at   :datetime
#  starts_at          :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  court_id           :bigint
#  run_schedule_id    :bigint
#
# Indexes
#
#  index_runs_on_court_id         (court_id)
#  index_runs_on_run_schedule_id  (run_schedule_id)
#
# Foreign Keys
#
#  fk_rails_...  (court_id => courts.id)
#  fk_rails_...  (run_schedule_id => run_schedules.id)
#
require "test_helper"

class RunTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
