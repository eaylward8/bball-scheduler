require "test_helper"

class RunTemplatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @run_template = run_templates(:one)
  end

  test "should get index" do
    get run_templates_url
    assert_response :success
  end

  test "should get new" do
    get new_run_template_url
    assert_response :success
  end

  test "should create run_template" do
    assert_difference("RunTemplate.count") do
      post run_templates_url, params: { run_template: {  } }
    end

    assert_redirected_to run_template_url(RunTemplate.last)
  end

  test "should show run_template" do
    get run_template_url(@run_template)
    assert_response :success
  end

  test "should get edit" do
    get edit_run_template_url(@run_template)
    assert_response :success
  end

  test "should update run_template" do
    patch run_template_url(@run_template), params: { run_template: {  } }
    assert_redirected_to run_template_url(@run_template)
  end

  test "should destroy run_template" do
    assert_difference("RunTemplate.count", -1) do
      delete run_template_url(@run_template)
    end

    assert_redirected_to run_templates_url
  end
end
