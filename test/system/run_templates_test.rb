require "application_system_test_case"

class RunTemplatesTest < ApplicationSystemTestCase
  setup do
    @run_template = run_templates(:one)
  end

  test "visiting the index" do
    visit run_templates_url
    assert_selector "h1", text: "Run templates"
  end

  test "should create run template" do
    visit run_templates_url
    click_on "New run template"

    click_on "Create Run template"

    assert_text "Run template was successfully created"
    click_on "Back"
  end

  test "should update Run template" do
    visit run_template_url(@run_template)
    click_on "Edit this run template", match: :first

    click_on "Update Run template"

    assert_text "Run template was successfully updated"
    click_on "Back"
  end

  test "should destroy Run template" do
    visit run_template_url(@run_template)
    click_on "Destroy this run template", match: :first

    assert_text "Run template was successfully destroyed"
  end
end
