Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  namespace :admin do
    resources :courts
    resources :reservations
    resources :run_schedules
    resources :runs
    resources :users

    root to: "users#index"
  end

  resources :courts
  # resources :reservations
  resources :runs do
    resources :reservations, shallow: true
  end
  resources :run_templates
  resources :users, only: [:index, :new, :create, :show, :edit, :update]

  get '/sign_up', to: 'users#new'

  root 'welcome#index'
end
