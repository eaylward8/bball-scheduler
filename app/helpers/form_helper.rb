# frozen_string_literal: true

module FormHelper
  def errors_for(form, field)
    return unless form.object.errors[field].present?

    content_tag(:span, form.object.errors[field].try(:first), class: "help is-danger")
  end
end
