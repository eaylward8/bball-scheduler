# frozen_string_literal: true

module ApplicationHelper
  def flash_class(key)
    return 'is-success' if key == 'success'
    return 'is-danger' if key == 'alert'

    'is-info'
  end
end
