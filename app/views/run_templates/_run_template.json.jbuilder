# frozen_string_literal: true

json.extract! run_template, :id, :created_at, :updated_at
json.url run_template_url(run_template, format: :json)
