# frozen_string_literal: true

json.partial! 'run_templates/run_template', run_template: @run_template
