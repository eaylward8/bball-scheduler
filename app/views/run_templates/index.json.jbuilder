# frozen_string_literal: true

json.array! @run_templates, partial: 'run_templates/run_template', as: :run_template
