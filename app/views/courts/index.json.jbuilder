# frozen_string_literal: true

json.array! @courts, partial: 'courts/court', as: :court
