# frozen_string_literal: true

json.partial! 'courts/court', court: @court
