# frozen_string_literal: true

json.extract! court, :id, :name, :street_address, :city, :state, :zip, :created_at, :updated_at
json.url court_url(court, format: :json)
