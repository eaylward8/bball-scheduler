# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id              :bigint           not null, primary key
#  admin           :boolean          default(FALSE), not null
#  commish         :boolean          default(FALSE), not null
#  email           :string           not null
#  first_name      :string           not null
#  last_name       :string           not null
#  nickname        :string
#  password_digest :string
#  phone           :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_users_on_email  (email) UNIQUE
#
class User < ApplicationRecord
  # rubocop:disable Layout/LineLength
  EMAIL_REGEXP = %r{\A[a-zA-Z0-9.!\#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+\z}
  # rubocop:enable Layout/LineLength

  has_secure_password

  validates :first_name, :last_name, :email, presence: true
  validates :email,
            uniqueness: { case_sensitive: false },
            format: {
              with: EMAIL_REGEXP,
              message: 'invalid email address'
            }

  def full_name
    "#{first_name} #{last_name}"
  end
end
