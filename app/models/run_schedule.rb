# frozen_string_literal: true

# == Schema Information
#
# Table name: run_schedules
#
#  id                      :bigint           not null, primary key
#  days_of_week            :integer          is an Array
#  end_time                :time
#  late_drop_deadline_day  :integer
#  late_drop_deadline_time :time
#  name                    :string           not null
#  player_limit            :integer
#  price_per_run_in_cents  :integer
#  repeat_duration         :daterange
#  repeat_interval         :string
#  sign_up_open_day        :string
#  sign_up_open_time       :time
#  start_time              :time
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  court_id                :bigint
#
# Indexes
#
#  index_run_schedules_on_court_id      (court_id)
#  index_run_schedules_on_days_of_week  (days_of_week) USING gin
#
# Foreign Keys
#
#  fk_rails_...  (court_id => courts.id)
#
class RunSchedule < ApplicationRecord
  TIME_FORMAT = '%I:%M %p'

  has_many :runs, dependent: :restrict_with_error
  belongs_to :court

  before_validation :remove_duplicate_days_of_week

  validates :name, :days_of_week, :start_time, :end_time, presence: true
  validates :name, length: { minimum: 3, maximum: 100 }
  validate :days_of_week_integers

  def start_time
    read_attribute(:start_time).strftime(TIME_FORMAT)
  end

  def end_time
    read_attribute(:end_time).strftime(TIME_FORMAT)
  end

  private

  def remove_duplicate_days_of_week
    days_of_week.uniq!
  end

  def days_of_week_integers
    return if days_of_week.all? { |i| (0..6).include?(i) }

    errors.add(:days_of_week, 'invalid value(s) for days of week')
  end
end
