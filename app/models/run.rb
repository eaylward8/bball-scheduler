# frozen_string_literal: true

# == Schema Information
#
# Table name: runs
#
#  id                 :bigint           not null, primary key
#  ends_at            :datetime
#  late_drop_deadline :datetime
#  name               :string           not null
#  player_limit       :integer
#  price_in_cents     :integer
#  sign_up_opens_at   :datetime
#  starts_at          :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  court_id           :bigint
#  run_schedule_id    :bigint
#
# Indexes
#
#  index_runs_on_court_id         (court_id)
#  index_runs_on_run_schedule_id  (run_schedule_id)
#
# Foreign Keys
#
#  fk_rails_...  (court_id => courts.id)
#  fk_rails_...  (run_schedule_id => run_schedules.id)
#
class Run < ApplicationRecord
  belongs_to :court
  belongs_to :run_schedule, optional: true

  validates :name, :starts_at, :ends_at, presence: true
  validate :starts_at_before_ends_at

  private

  def starts_at_before_ends_at
    return if starts_at < ends_at

    errors.add(:start_time, "can't be after end time")
  end
end
