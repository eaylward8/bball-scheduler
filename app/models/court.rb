# frozen_string_literal: true

# == Schema Information
#
# Table name: courts
#
#  id             :bigint           not null, primary key
#  city           :string
#  name           :string           not null
#  state          :string
#  street_address :string
#  zip            :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_courts_on_name  (name) UNIQUE
#
class Court < ApplicationRecord
  has_many :run_templates, dependent: :restrict_with_error
  has_many :runs, dependent: :restrict_with_error

  validates :name, presence: true, uniqueness: true, length: { minimum: 3, maximum: 50 }
end
