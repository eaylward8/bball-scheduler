# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :under_construction, if: -> { Rails.env.production? }

  private

  def under_construction
    messages = %w[game handles jumper crossover passing]
    render html: helpers.tag.h1("Working on my #{messages.sample}. Check back later.")
  end
end
