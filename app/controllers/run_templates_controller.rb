# frozen_string_literal: true

class RunTemplatesController < ApplicationController
  before_action :set_run_template, only: %i[show edit update destroy]

  # GET /run_templates or /run_templates.json
  def index
    @run_templates = RunTemplate.all
  end

  # GET /run_templates/1 or /run_templates/1.json
  def show; end

  # GET /run_templates/new
  def new
    @run_template = RunTemplate.new
  end

  # GET /run_templates/1/edit
  def edit; end

  # POST /run_templates or /run_templates.json
  def create
    @run_template = RunTemplate.new(run_template_params)

    respond_to do |format|
      if @run_template.save
        format.html { redirect_to run_template_url(@run_template), notice: 'Run template was successfully created.' }
        format.json { render :show, status: :created, location: @run_template }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @run_template.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /run_templates/1 or /run_templates/1.json
  def update
    respond_to do |format|
      if @run_template.update(run_template_params)
        format.html { redirect_to run_template_url(@run_template), notice: 'Run template was successfully updated.' }
        format.json { render :show, status: :ok, location: @run_template }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @run_template.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /run_templates/1 or /run_templates/1.json
  def destroy
    @run_template.destroy

    respond_to do |format|
      format.html { redirect_to run_templates_url, notice: 'Run template was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_run_template
    @run_template = RunTemplate.find(params[:id])
  end

  def run_template_params
    # params.fetch(:run_template, {})
    params.require(:run_template).permit(
      :name,
      :court_id,
      :player_limit,
      :total_price_in_dollars,
      :day_of_week,
      :start_hour,
      :start_minute,
      :end_hour,
      :end_minute
    )
  end
end
