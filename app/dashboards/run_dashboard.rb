require "administrate/base_dashboard"

class RunDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    court: Field::BelongsTo,
    run_schedule: Field::BelongsTo,
    id: Field::Number,
    name: Field::String,
    starts_at: Field::DateTime,
    ends_at: Field::DateTime,
    player_limit: Field::Number,
    sign_up_opens_at: Field::DateTime,
    late_drop_deadline: Field::DateTime,
    price_in_cents: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = %i[
    court
    run_schedule
    id
    name
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = %i[
    court
    run_schedule
    id
    name
    starts_at
    ends_at
    player_limit
    sign_up_opens_at
    late_drop_deadline
    price_in_cents
    created_at
    updated_at
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = %i[
    court
    run_schedule
    name
    starts_at
    ends_at
    player_limit
    sign_up_opens_at
    late_drop_deadline
    price_in_cents
  ].freeze

  # COLLECTION_FILTERS
  # a hash that defines filters that can be used while searching via the search
  # field of the dashboard.
  #
  # For example to add an option to search for open resources by typing "open:"
  # in the search field:
  #
  #   COLLECTION_FILTERS = {
  #     open: ->(resources) { resources.where(open: true) }
  #   }.freeze
  COLLECTION_FILTERS = {}.freeze

  # Overwrite this method to customize how runs are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(run)
  #   "Run ##{run.id}"
  # end
end
