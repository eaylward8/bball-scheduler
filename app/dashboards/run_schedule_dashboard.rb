require "administrate/base_dashboard"

class RunScheduleDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    runs: Field::HasMany,
    court: Field::BelongsTo,
    id: Field::Number,
    name: Field::String,
    days_of_week: Field::Number,
    start_time: Field::Time,
    end_time: Field::Time,
    player_limit: Field::Number,
    sign_up_open_day: Field::String,
    sign_up_open_time: Field::Time,
    late_drop_deadline_day: Field::Number,
    late_drop_deadline_time: Field::Time,
    price_per_run_in_cents: Field::Number,
    repeat_interval: Field::String,
    repeat_duration: Field::String.with_options(searchable: false),
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = %i[
    runs
    court
    id
    name
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = %i[
    runs
    court
    id
    name
    days_of_week
    start_time
    end_time
    player_limit
    sign_up_open_day
    sign_up_open_time
    late_drop_deadline_day
    late_drop_deadline_time
    price_per_run_in_cents
    repeat_interval
    repeat_duration
    created_at
    updated_at
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = %i[
    runs
    court
    name
    days_of_week
    start_time
    end_time
    player_limit
    sign_up_open_day
    sign_up_open_time
    late_drop_deadline_day
    late_drop_deadline_time
    price_per_run_in_cents
    repeat_interval
    repeat_duration
  ].freeze

  # COLLECTION_FILTERS
  # a hash that defines filters that can be used while searching via the search
  # field of the dashboard.
  #
  # For example to add an option to search for open resources by typing "open:"
  # in the search field:
  #
  #   COLLECTION_FILTERS = {
  #     open: ->(resources) { resources.where(open: true) }
  #   }.freeze
  COLLECTION_FILTERS = {}.freeze

  # Overwrite this method to customize how run schedules are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(run_schedule)
  #   "RunSchedule ##{run_schedule.id}"
  # end
end
