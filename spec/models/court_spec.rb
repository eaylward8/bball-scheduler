# == Schema Information
#
# Table name: courts
#
#  id             :bigint           not null, primary key
#  city           :string
#  name           :string           not null
#  state          :string
#  street_address :string
#  zip            :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_courts_on_name  (name) UNIQUE
#
require 'rails_helper'

RSpec.describe Court, type: :model do
  it 'has a valid factory' do
    expect(build(:court)).to be_valid
  end

  describe 'validations' do
    describe 'name' do
      it 'must be unique' do
        create(:court, name: 'Rucker Park')
        expect(build(:court, name: 'Rucker Park')).to be_invalid
      end

      it 'must be at least 3 characters' do
        expect(build(:court, name: 'XX')).to be_invalid
      end

      it 'must be no more than 50 characters' do
        expect(build(:court, name: 'X' * 51)).to be_invalid
      end
    end
  end
end
