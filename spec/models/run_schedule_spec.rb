# == Schema Information
#
# Table name: run_schedules
#
#  id                      :bigint           not null, primary key
#  days_of_week            :integer          is an Array
#  end_time                :time
#  late_drop_deadline_day  :integer
#  late_drop_deadline_time :time
#  name                    :string           not null
#  player_limit            :integer
#  price_per_run_in_cents  :integer
#  repeat_duration         :daterange
#  repeat_interval         :string
#  sign_up_open_day        :string
#  sign_up_open_time       :time
#  start_time              :time
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  court_id                :bigint
#
# Indexes
#
#  index_run_schedules_on_court_id      (court_id)
#  index_run_schedules_on_days_of_week  (days_of_week) USING gin
#
# Foreign Keys
#
#  fk_rails_...  (court_id => courts.id)
#
require 'rails_helper'

RSpec.describe RunSchedule, type: :model do
  it 'has a valid factory' do
    expect(build(:run_schedule)).to be_valid
  end

  describe 'validations' do
    describe 'name' do
      it 'must be at least 3 characters' do
        expect(build(:run_schedule, name: 'ab')).to be_invalid
      end

      it 'must be no more than 100 characters' do
        expect(build(:run_schedule, name: 'x' * 101)).to be_invalid
      end
    end

    describe 'days_of_week' do
      it 'is valid with integers from 0 through 6' do
        expect(build(:run_schedule, days_of_week: [0, 1, 2, 3, 4, 5, 6])).to be_valid
      end

      it 'is invalid with integers outside of 0 through 6' do
        expect(build(:run_schedule, days_of_week: [7])).to be_valid
      end
    end
  end
end
