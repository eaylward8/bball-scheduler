# == Schema Information
#
# Table name: runs
#
#  id                 :bigint           not null, primary key
#  ends_at            :datetime
#  late_drop_deadline :datetime
#  name               :string           not null
#  player_limit       :integer
#  price_in_cents     :integer
#  sign_up_opens_at   :datetime
#  starts_at          :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  court_id           :bigint
#  run_schedule_id    :bigint
#
# Indexes
#
#  index_runs_on_court_id         (court_id)
#  index_runs_on_run_schedule_id  (run_schedule_id)
#
# Foreign Keys
#
#  fk_rails_...  (court_id => courts.id)
#  fk_rails_...  (run_schedule_id => run_schedules.id)
#
require 'rails_helper'

RSpec.describe Run, type: :model do
  it 'has a valid factory' do
    expect(build(:run)).to be_valid
  end

  describe 'validations' do
    let(:run) { build(:run, starts_at: Time.zone.now, ends_at: 1.minute.from_now) }

    it 'is valid if starts_at is before ends_at' do
      expect(run).to be_valid
    end

    it 'is invalid if starts_at is the same or later than ends_at' do
      run.starts_at = run.ends_at
      expect(run).to be_invalid
    end
  end
end
