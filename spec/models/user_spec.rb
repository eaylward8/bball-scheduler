# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id              :bigint           not null, primary key
#  admin           :boolean          default(FALSE), not null
#  commish         :boolean          default(FALSE), not null
#  email           :string           not null
#  first_name      :string           not null
#  last_name       :string           not null
#  nickname        :string
#  password_digest :string
#  phone           :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_users_on_email  (email) UNIQUE
#
require 'rails_helper'

RSpec.describe User, type: :model do
  it 'has a valid factory' do
    expect(build(:user)).to be_valid
  end

  describe 'validations' do
    it { expect(build(:user, first_name: nil)).to be_invalid }
    it { expect(build(:user, last_name: nil)).to be_invalid }
    it { expect(build(:user, email: nil)).to be_invalid }

    describe 'email' do
      it 'must be unique (case-insensitive)' do
        create(:user, email: 'chuck@test.com')
        expect(build(:user, email: 'CHUCK@test.com')).to be_invalid
      end

      describe 'formatting' do
        it { expect(build(:user, email: 'shaq@test.com')).to be_valid }
        it { expect(build(:user, email: 'shaq')).to be_invalid }
        it { expect(build(:user, email: 'shaq@test')).to be_invalid }
        it { expect(build(:user, email: 'shaq@.com')).to be_invalid }
      end
    end
  end
end
