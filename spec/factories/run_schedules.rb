# == Schema Information
#
# Table name: run_schedules
#
#  id                      :bigint           not null, primary key
#  days_of_week            :integer          is an Array
#  end_time                :time
#  late_drop_deadline_day  :integer
#  late_drop_deadline_time :time
#  name                    :string           not null
#  player_limit            :integer
#  price_per_run_in_cents  :integer
#  repeat_duration         :daterange
#  repeat_interval         :string
#  sign_up_open_day        :string
#  sign_up_open_time       :time
#  start_time              :time
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  court_id                :bigint
#
# Indexes
#
#  index_run_schedules_on_court_id      (court_id)
#  index_run_schedules_on_days_of_week  (days_of_week) USING gin
#
# Foreign Keys
#
#  fk_rails_...  (court_id => courts.id)
#
FactoryBot.define do
  factory :run_schedule do
    court
    name { Faker::Hipster.words(number: 3).join(' ').capitalize }
    days_of_week { [*0..6].delete_if { [true, false].sample } }
    start_time { Time.zone.parse("#{[*5..19].sample}:#{%w[00 15 30 45].sample}") }
    end_time { start_time + [1.0, 1.5, 2.0, 2.5].sample.hours }
  end
end
