# == Schema Information
#
# Table name: courts
#
#  id             :bigint           not null, primary key
#  city           :string
#  name           :string           not null
#  state          :string
#  street_address :string
#  zip            :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_courts_on_name  (name) UNIQUE
#
FactoryBot.define do
  factory :court do
    name { "#{Faker::Address.street_name} #{%w[Court Gym].sample}" }
    street_address { Faker::Address.street_address }
    city { Faker::Address.city }
    state { Faker::Address.state_abbr }
    zip { Faker::Address.zip }
  end
end
