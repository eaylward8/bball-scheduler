# == Schema Information
#
# Table name: runs
#
#  id                 :bigint           not null, primary key
#  ends_at            :datetime
#  late_drop_deadline :datetime
#  name               :string           not null
#  player_limit       :integer
#  price_in_cents     :integer
#  sign_up_opens_at   :datetime
#  starts_at          :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  court_id           :bigint
#  run_schedule_id    :bigint
#
# Indexes
#
#  index_runs_on_court_id         (court_id)
#  index_runs_on_run_schedule_id  (run_schedule_id)
#
# Foreign Keys
#
#  fk_rails_...  (court_id => courts.id)
#  fk_rails_...  (run_schedule_id => run_schedules.id)
#
FactoryBot.define do
  factory :run do
    court
    name { "#{Faker::Games::DnD.city} Run" }
    starts_at { Faker::Time.between(from: 3.days.ago, to: 3.days.from_now).at_beginning_of_hour }
    ends_at { starts_at + [1.0, 1.5, 2.0, 2.5].sample.hours }

    factory :run_with_schedule do
      run_schedule
    end
  end
end
